# REST (Representional State Transfer) 
Metode komunikasi yang menggunakan protokol HTTP untuk pertukaran data biasa diterapkan dalam pengembangan aplikasi.

# JPA (Java Persistence API)
JPA menggunakan teknologi ORM
JPA merupakan standart ORM dalam java
Memanipulasi data tanpa query

# ORM (Object Relationship Mapping)
Teknik untuk mengkonversi data dari lingkungan pemrogrman ke dalam lingkungan database relasional.

@Entity // anotasi untuk mapping ke database
---
@Table (name="tb_contoh") // nama tabel
---
@Id // anotasi sebagai primary key
---
@Column (name="no_id",nullable=false) // nama db "no_id" dan tidak boleh kosong
---
private int no_id;

## > desc tb_contoh;
+-----------------+
|no_id | not null |
+-----------------+
---
@Autowired digunakan untuk menginjeksikan session yang akan dipanggil melalui applicationContext untuk berhubungan dengan database.
@Transactional   = Menandakan Transaksi antara class dengan database 
@SuppresWarnings  = Memberi tahu pada compailer untuk mengabaikan warning

# DAO (Data Access Object)
Biasa disebut "controller" bukan tentang MVC
Konsep DAO, semua entitas tersebut harus memiliki DAO masing-masing
Menggunakan teknologi Depedency Injection

# DI (Dependency Injection)
Melakukan connection dan datasource (DB, User, PW) tanpa membuat inisialisasi object manual.
Caranya dengan menambahkan @Autowired



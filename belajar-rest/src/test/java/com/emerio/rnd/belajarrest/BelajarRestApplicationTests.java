package com.emerio.rnd.belajarrest;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import com.emerio.rnd.belajarrest.entity.Biodata;
import com.emerio.rnd.belajarrest.repo.BiodataRepo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BelajarRestApplication.class)
@WebAppConfiguration
public class BelajarRestApplicationTests {
	/*
	 * @Test public void contextLoads() { }
	 */

	// Tambah code testing

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mockMvc;

	private String URIbiodata = "/biodata";

	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private BiodataRepo biodataRepo;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	private Biodata biodata1;
	private Biodata biodata2;
	private Biodata biodata3;
	private Biodata biodata3_1;
	private Biodata biodata3_2;
	private Biodata biodata4;

	@Before()
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		this.biodata1 = new Biodata();
		this.biodata1.setUsername("username1");
		this.biodata1.setFirstname("firstname1");
		this.biodata1.setLastname("lastname1");
		this.biodata1.setAddress("depok");

		this.biodata2 = new Biodata();
		this.biodata2.setUsername("username2");
		this.biodata2.setFirstname("firstname2");
		this.biodata2.setLastname("lastname2");
		this.biodata2.setAddress("depok");
		// untuk di tampilkan salah satu id
		this.biodata2 = biodataRepo.save(biodata2);

		this.biodata3 = new Biodata();
		this.biodata3.setUsername("username3");
		this.biodata3.setFirstname("firstname3");
		this.biodata3.setLastname("lastname3");
		this.biodata3.setAddress("depok3");

		this.biodata3_1 = new Biodata();
		this.biodata3_1.setLastname("username_pengganti_biodata2");

		this.biodata3_2 = new Biodata();
		this.biodata3_2.setUsername("username3_2");
		this.biodata3_2.setFirstname("firstname3_2");
		this.biodata3_2.setLastname("");
		this.biodata3_2.setAddress("depok3_2");

		this.biodata4 = new Biodata();
		this.biodata4.setUsername("username4");
		// this.biodata4.setFirstname("");
		this.biodata4.setLastname("lastname3");
		this.biodata4.setAddress("depok");
		// this.biodata4 = biodataRepo.save(biodata4);
	}

	@Test
	public void workingCode() throws Exception{
		BelajarRestApplication.main(new String [] {});
	}

	// Positive Test
	// untuk memasukkan data
	@Test
	public void createUser() throws Exception {
		mockMvc.perform(post(this.URIbiodata).content(this.json(biodata1)).contentType(contentType))
				.andExpect(status().isCreated());
	}

	// untuk menampilkan semua data
	@Test
	public void readUserAll() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIbiodata));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(status().isOk());
	}

	// untuk menampilkan data sesuai id
	@Test
	public void readUserA() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIbiodata + "/" + this.biodata2.getId()));
		ra.andExpect(content().json(this.json(biodata2)));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(status().isOk());
	}

	// update semua atribut data
	@Test
	public void updateSemuaAtribut() throws Exception {
		mockMvc.perform(put(this.URIbiodata + "/" + this.biodata2.getId()).content(this.json(biodata3))
				.contentType(contentType)).andExpect(status().isCreated());
	}

	// update salah satu atribut saja
	@Test
	public void update_1_Atribut() throws Exception {
		mockMvc.perform(put(this.URIbiodata + "/" + this.biodata2.getId()).content(this.json(biodata3_1))
				.contentType(contentType)).andExpect(status().isCreated());
	}

	// update dengan lastname kosangkan
	@Test
	public void update_lastname_kosong() throws Exception {
		mockMvc.perform(put(this.URIbiodata + "/" + this.biodata2.getId()).content(this.json(biodata3_2))
				.contentType(contentType)).andExpect(status().isCreated());
	}

	// delete id yang terdaftar
	@Test
	public void hapus_id_terdaftar() throws Exception {
		ResultActions ra = mockMvc.perform(delete(this.URIbiodata + "/" + this.biodata2.getId()));
		ra.andExpect(status().isOk());
	}

	// Negative Test
	// tidak memasukkan username / firstname / address
	@Test
	public void createUser2() throws Exception {
		mockMvc.perform(post(this.URIbiodata).content(this.json(biodata4)).contentType(contentType))
				.andExpect(status().isBadRequest());
	}

	// untuk menampilkan id yang blm terdaftar
	@Test
	public void readUserA2() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIbiodata + "/" + "10"));
		ra.andExpect(status().isNotFound());
	}

	// request method get/id but null
	@Test
	public void readUserNull() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIbiodata + "/null"));
		ra.andExpect(status().isBadRequest());
	}

	// update salah satu atribut not null
	@Test
	public void updateUserNull() throws Exception {
		mockMvc.perform(put(this.URIbiodata + "/null").content(this.json(biodata2))
				.contentType(contentType))
				.andExpect(status().isBadRequest());
	}


	// delete id yang Tidak terdaftar
	@Test
	public void hapus_id_tidak_terdaftar() throws Exception {
		ResultActions ra = mockMvc.perform(delete(this.URIbiodata + "/0"));
		ra.andExpect(status().isNotFound());
	}

	// method delete/id but null
	@Test
	public void deleteNull() throws Exception {
		ResultActions rada = mockMvc.perform(delete(this.URIbiodata + "/null"));
		rada.andExpect(status().isBadRequest());
	}

	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
		jsonConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}
